package com.yazhe.ordermeal;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.yazhe.ordermeal.fragment.CartFragment;
import com.yazhe.ordermeal.fragment.HomeFragment;
import com.yazhe.ordermeal.fragment.MyFragment;
import com.yazhe.ordermeal.fragment.OrderFragment;

public class MainActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;

    private FragmentTransaction mFragmentTransaction;

    private HomeFragment mHomeFragment;
    private CartFragment mCartFragment;
    private OrderFragment mOrderFragment;
    private MyFragment  mMyFragment;

    private int index =0;

    private RadioGroup  maingroup;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        maingroup = findViewById(R.id.maingroup);
        maingroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId){
                    case R.id.radio_home:
                        setFragment(0);
                        break;
                    case R.id.radio_cart:
                        setFragment(1);
                        break;
                    case R.id.radio_order:
                        setFragment(2);
                        break;
                    case R.id.radio_my:
                        setFragment(3);
                        break;
                }
            }
        });


        setFragment(index);
    }

    private void setFragment(int index) {

        mFragmentManager =getSupportFragmentManager();
        mFragmentTransaction =mFragmentManager.beginTransaction();
        hideFragment();

        switch (index){
            case 0:
                if(mHomeFragment==null){
                    mHomeFragment = new HomeFragment();

                    mFragmentTransaction.add(R.id.fragment_content,mHomeFragment);
                }else{
                    mFragmentTransaction.show(mHomeFragment);
                }
                break;
            case 1:
                if(mCartFragment==null){
                    mCartFragment = new CartFragment();

                    mFragmentTransaction.add(R.id.fragment_content,mCartFragment);
                }else{
                    mFragmentTransaction.show(mCartFragment);
                }
                break;
            case 2:
                if(mOrderFragment==null){
                    mOrderFragment = new OrderFragment();

                    mFragmentTransaction.add(R.id.fragment_content,mOrderFragment);
                }else{
                    mFragmentTransaction.show(mOrderFragment);
                }
                break;
            case 3:
                if(mMyFragment==null){
                    mMyFragment = new MyFragment();

                    mFragmentTransaction.add(R.id.fragment_content,mMyFragment);
                }else{
                    mFragmentTransaction.show(mMyFragment);
                }
                break;
        }
        //提交事务
        mFragmentTransaction.commit();
    }
    private void hideFragment(){

        if(mHomeFragment!=null){
            mFragmentTransaction.hide(mHomeFragment);
        }
        if(mCartFragment!=null){
            mFragmentTransaction.hide(mCartFragment);
        }
        if(mOrderFragment!=null){
            mFragmentTransaction.hide(mOrderFragment);
        }
        if(mMyFragment!=null){
            mFragmentTransaction.hide(mMyFragment);
        }



    }


}
