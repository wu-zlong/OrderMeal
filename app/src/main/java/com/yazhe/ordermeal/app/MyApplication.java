package com.yazhe.ordermeal.app;

import android.app.Application;

import com.kongzue.dialogx.DialogX;
import com.kongzue.dialogx.style.IOSStyle;
import com.yazhe.ordermeal.pojo.UserInfo;

public class MyApplication extends Application {
    public static int dataBaseVersion =1;

    public  static UserInfo sUserInfo;
    @Override
    public void onCreate() {
        super.onCreate();
        //弹出框初始化
        DialogX.init(this);
        //设置为IOS主题
        DialogX.globalStyle = new IOSStyle();
    }
}
