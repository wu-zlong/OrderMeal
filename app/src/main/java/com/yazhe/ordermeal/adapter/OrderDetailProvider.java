package com.yazhe.ordermeal.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.pojo.OrderBase;
import com.yazhe.ordermeal.pojo.OrderBaseDetail;
import com.yazhe.ordermeal.view.CustomTextView;

import org.jetbrains.annotations.NotNull;

public class OrderDetailProvider extends BaseNodeProvider {
    @Override
    public int getItemViewType() {
        return 2;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_orderdetailprovider;
    }

    @Override
    public void convert(@NotNull BaseViewHolder baseViewHolder, BaseNode baseNode) {
        OrderBaseDetail orderBaseDetail = (OrderBaseDetail) baseNode;

        if (orderBaseDetail.getMenuimage()!= null){
            Bitmap bitmap = BitmapFactory.decodeFile(orderBaseDetail.getMenuimage());

            baseViewHolder.setImageBitmap(R.id.menuinages,bitmap);
        }

          baseViewHolder.setText(R.id.menuname,orderBaseDetail.getMenuname());
          baseViewHolder.setText(R.id.menunum,orderBaseDetail.getMenunum()+"");
         baseViewHolder.setText(R.id.menuprice,orderBaseDetail.getPrice()+"元");
    }
}
