package com.yazhe.ordermeal.adapter;

import com.chad.library.adapter.base.BaseNodeAdapter;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.yazhe.ordermeal.pojo.OrderBase;
import com.yazhe.ordermeal.pojo.OrderBaseDetail;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class OrderAdapter extends BaseNodeAdapter implements LoadMoreModule {


    public OrderAdapter() {
        addNodeProvider(new OrderProvider());
        addNodeProvider(new OrderDetailProvider());
    }

    @Override
    protected int getItemType(@NotNull List<? extends BaseNode> list, int i) {

            BaseNode  node = list.get(i);

            if (node instanceof OrderBase){

                return 1;
            }else if (node instanceof OrderBaseDetail) {
                return 2;
            }
           return -1;




    }
}
