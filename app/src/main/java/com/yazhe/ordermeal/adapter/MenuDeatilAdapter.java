package com.yazhe.ordermeal.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.pojo.CommentList;
import com.yazhe.ordermeal.pojo.Menu;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class MenuDeatilAdapter extends BaseQuickAdapter<CommentList, BaseViewHolder> implements LoadMoreModule {
    public MenuDeatilAdapter(int layoutResId) {
        super(layoutResId);

    }
    public MenuDeatilAdapter(int layoutResId, @Nullable List<CommentList> data) {
        super(layoutResId, data);
    }


    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, CommentList commentList) {
        if (commentList.getPhotoimage() != null){
            Bitmap bitmap = BitmapFactory.decodeFile(commentList.getPhotoimage());

            baseViewHolder.setImageBitmap(R.id.imagephoto,bitmap);
        }

                baseViewHolder.setText(R.id.nickname,commentList.getNickname());
                baseViewHolder.setText(R.id.commentdate,commentList.getCommentdate());
                baseViewHolder.setText(R.id.commentcontent,commentList.getCommentcontent());
    }
}
