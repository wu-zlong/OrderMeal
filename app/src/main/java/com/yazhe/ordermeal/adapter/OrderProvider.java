package com.yazhe.ordermeal.adapter;

import android.view.View;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.pojo.OrderBase;
import com.yazhe.ordermeal.view.CustomTextView;

import org.jetbrains.annotations.NotNull;

public class OrderProvider extends BaseNodeProvider {
    @Override
    public int getItemViewType() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_orderprovider;
    }

    @Override
    public void convert(@NotNull BaseViewHolder baseViewHolder, BaseNode baseNode) {
        OrderBase  orderBase = (OrderBase) baseNode;
              baseViewHolder.setText(R.id.orderid,orderBase.getOrderid());
              baseViewHolder.setText(R.id.ordertime,orderBase.getOrdercreate());
              baseViewHolder.setText(R.id.orderprice,orderBase.getOrderprice()+"元");



    }


    @Override
    public void onClick(@NotNull BaseViewHolder helper, @NotNull View view, BaseNode data, int position) {
        OrderBase  orderBase = (OrderBase) data;
            if (orderBase.isExpanded()){
                getAdapter().collapse(position);
            }else{
                getAdapter().expandAndCollapseOther(position);
            }
    }
}
