package com.yazhe.ordermeal.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.pojo.Cart;
import com.yazhe.ordermeal.pojo.CartInfo;

import java.util.List;

public class CartAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<CartInfo> mlist;

    private MyCallBack myCallBack;

    public CartAdapter(Context context,List<CartInfo> list,MyCallBack myCallBack){
        mLayoutInflater =LayoutInflater.from(context);
        mContext =context;
        mlist =list;
        this.myCallBack =myCallBack;

    }


    @Override
    public int getCount() {
        if (mlist.size()>0){
            return mlist.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView==null){
                convertView = mLayoutInflater.inflate(R.layout.item_cart,parent,false);

            viewHolder = new ViewHolder();

            viewHolder.menuImage = convertView.findViewById(R.id.menuimage);
            viewHolder.menuname = convertView.findViewById(R.id.menuname);
            viewHolder.price = convertView.findViewById(R.id.price);
            viewHolder.menunum = convertView.findViewById(R.id.menunum);
            viewHolder.cartitemcheck =convertView.findViewById(R.id.cartitemcheck);

            convertView.setTag(viewHolder);


        }else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        if (mlist.get(position).getMenuimage()!=null){
            Bitmap bitmap = BitmapFactory.decodeFile(mlist.get(position).getMenuimage());
            viewHolder.menuImage.setImageBitmap(bitmap);
        }

        viewHolder.menuname.setText(mlist.get(position).getMenuname());
        viewHolder.menunum.setText(mlist.get(position).getMenunum()+"");
        viewHolder.price.setText(mlist.get(position).getPrice()+"");
        viewHolder.cartitemcheck.setChecked(mlist.get(position).isChecked());
        viewHolder.cartitemcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              if (mlist.get(position).isChecked()) {
                  mlist.get(position).setChecked(false);

              }else{
                  mlist.get(position).setChecked(true);
              }
              myCallBack.onTtemWidgetClickListener(v);
            }
        });


        return convertView;
    }

  private final class ViewHolder{

        ImageView menuImage;
        TextView menuname,price,menunum;
        CheckBox cartitemcheck;

  }


  public interface MyCallBack{
        public  void onTtemWidgetClickListener(View v);
  }
}
