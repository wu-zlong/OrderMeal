package com.yazhe.ordermeal.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.pojo.Menu;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class HomeAdapter extends BaseQuickAdapter<Menu, BaseViewHolder> implements LoadMoreModule {
    public HomeAdapter(int layoutResId) {
        super(layoutResId);
        addChildClickViewIds(R.id.addCard);
    }
    public HomeAdapter(int layoutResId, @Nullable List<Menu> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Menu menu) {
        if (menu.getImags()!= null){
            Bitmap bitmap = BitmapFactory.decodeFile(menu.getImags());

            baseViewHolder.setImageBitmap(R.id.menuimages,bitmap);
        }

            baseViewHolder.setText(R.id.menuname,menu.getMenuname());
            baseViewHolder.setText(R.id.price,menu.getPrice()+"元");
            baseViewHolder.setText(R.id.menuInfo, menu.getMenuInfo());

    }
}
