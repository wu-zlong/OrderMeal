package com.yazhe.ordermeal;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.yazhe.ordermeal.adapter.HomeAdapter;
import com.yazhe.ordermeal.adapter.MenuDeatilAdapter;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.dao.MenuDao;
import com.yazhe.ordermeal.pojo.Cart;
import com.yazhe.ordermeal.pojo.Comment;
import com.yazhe.ordermeal.pojo.CommentList;
import com.yazhe.ordermeal.pojo.Menu;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;
import com.yazhe.ordermeal.view.CustomTextView;

import java.util.List;

public class MenuDetailActivity extends AppCompatActivity {

    private ImageView  menuImage;
    private TextView menuname,menuprice,addcart,synopsis;
    private Menu menu;
    private MenuDao mMenuDao;
    private RecyclerView commentRecycle;
    private SwipeRefreshLayout refreshLayout;

    private MenuDeatilAdapter mMenuDeatilAdapter;

    private List<CommentList> mCommentLists;
    private CustomTextView titlebar;

    private int pageSize =10;
    private int pageNum =1;

    private Button send;
    private EditText comment;

    private Handler mHandler = new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {

            switch (msg.what){
                case 1:
                    comment.setText("");
                    refreshLayout.setRefreshing(false);

                    mMenuDeatilAdapter.getLoadMoreModule().setEnableLoadMore(true);
                    if(pageNum==1){
                        mMenuDeatilAdapter.setList(mCommentLists);
                    }else{
                        mMenuDeatilAdapter.addData(mCommentLists);
                    }
                    if(mCommentLists.size()<pageSize){
                        mMenuDeatilAdapter.getLoadMoreModule().loadMoreEnd();

                    }else{
                        mMenuDeatilAdapter.getLoadMoreModule().loadMoreComplete();
                    }

                    pageNum++;

                    break;
            }
        }
    };
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_men_detail);
         menu = (Menu) getIntent().getSerializableExtra("menu");
        DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(this,"ordermeal.db",null, MyApplication.dataBaseVersion);
        dictionSQLiteOpenHelper.getWritableDatabase().close();

        mMenuDao = new MenuDao(dictionSQLiteOpenHelper);

        Log.d("数据",menu.toString());
       initdata();
        init();





    }

    private void initdata(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                pageNum =1;
                mCommentLists =   mMenuDao.selectCommentPage(pageSize,(pageNum-1)*pageSize,menu.getId());
                Message message = new Message();
                message.what =1;
                mHandler.sendMessage(message);
            }
        }).start();
    }


    private void init(){
        comment = findViewById(R.id.comment);
        send = findViewById(R.id.send);
        titlebar = findViewById(R.id.titlebar);
        menuImage = findViewById(R.id.menuimages);
        menuname = findViewById(R.id.menuname);
        menuprice = findViewById(R.id.menuprice);
        addcart = findViewById(R.id.addcart);
        synopsis = findViewById(R.id.synopsis);
        menuname.setText(menu.getMenuname());
        menuprice.setText(menu.getPrice()+"元");
        synopsis.setText(menu.getMenuInfo());

        if (menu.getImags()!=null){
            Bitmap bitmap = BitmapFactory.decodeFile(menu.getImags());
            menuImage.setImageBitmap(bitmap);
        }

        addcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart cart = new Cart();
                cart.setUsername(MyApplication.sUserInfo.getUsername());
                cart.setMenuid(menu.getId());
                cart.setMenunum(1);

                Cart cart1 =   mMenuDao.selectByuserNameAndMenuId(MyApplication.sUserInfo.getUsername(),menu.getId());


                long row =0L;
                if (cart1!=null){
                    Log.d("修改","修改");
                    cart.setMenunum(cart1.getMenunum()+1);
                    row =   mMenuDao.updatecartnum(cart);

                }else{
                    row = mMenuDao.insertCart(cart);
                    Log.d("添加","添加");
                }


                if (row>0) {
                    WaitDialog.dismiss();
                    TipDialog.show("添加成功", WaitDialog.TYPE.SUCCESS,2000);

                }else{
                    WaitDialog.dismiss();
                    TipDialog.show("添加失败", WaitDialog.TYPE.ERROR,2000);
                }


            }
        });

        refreshLayout =  findViewById(R.id.refreshLayout);
        commentRecycle =  findViewById(R.id.commentrecycle);
        commentRecycle.setLayoutManager(new LinearLayoutManager(this));
        commentRecycle.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mMenuDeatilAdapter = new MenuDeatilAdapter(R.layout.item_comment);
        mMenuDeatilAdapter.setList(mCommentLists);

        commentRecycle.setAdapter(mMenuDeatilAdapter);
        initRefreshLayout();
        initLoadMore();
        titlebar.setOnTextViewClickListener(new CustomTextView.OnTextViewClickListener(){
            @Override
            public void OnLeftImgClick() {
                super.OnLeftImgClick();
                finish();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentcontent = comment.getText().toString().trim();
                Comment comments = new Comment();
                comments.setCommentcontent(commentcontent);
                comments.setMenuid(menu.getId());
                comments.setUsername(MyApplication.sUserInfo.getUsername());

               Long row = mMenuDao.insertComment(comments);
               if (row>0){
                   new Thread(new Runnable() {
                       @Override
                       public void run() {
                           pageNum =1;
                           mCommentLists =   mMenuDao.selectCommentPage(pageSize,(pageNum-1)*pageSize,menu.getId());
                           Message message = new Message();
                           message.what =1;
                           mHandler.sendMessage(message);
                       }
                   }).start();

               }else{

               }
            }
        });
    }

    /**
     * 下拉刷新
     */
    private void initRefreshLayout(){
        refreshLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mMenuDeatilAdapter.getLoadMoreModule().setEnableLoadMore(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        pageNum =1;
                        mCommentLists =   mMenuDao.selectCommentPage(pageSize,(pageNum-1)*pageSize,menu.getId());
                        Message message = new Message();
                        message.what =1;
                        mHandler.sendMessage(message);
                    }
                }).start();


            }
        });
    }

    /**
     * 上拉加载
     */
    private void initLoadMore(){
        mMenuDeatilAdapter.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                refreshLayout.setRefreshing(false);
                mMenuDeatilAdapter.getLoadMoreModule().setEnableLoadMore(true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mCommentLists =   mMenuDao.selectCommentPage(pageSize,(pageNum-1)*pageSize,menu.getId());
                        Message message = new Message();
                        message.what =1;
                        mHandler.sendMessage(message);
                    }
                }).start();

            }
        });
        mMenuDeatilAdapter.getLoadMoreModule().setAutoLoadMore(true);
        mMenuDeatilAdapter.getLoadMoreModule().setEnableLoadMoreIfNotFullPage(false);
    }
}
