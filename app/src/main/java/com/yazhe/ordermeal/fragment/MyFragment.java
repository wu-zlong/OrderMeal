package com.yazhe.ordermeal.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.yazhe.ordermeal.LoginActivity;
import com.yazhe.ordermeal.MenuUpLoadActivity;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.UpdateUserinfoActivity;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.view.CustomTextView;

public class MyFragment extends Fragment {
    private View rootView;

    private ImageView imagephoto;
    private TextView username;

    private Button  btn_login;

    private CustomTextView customTextView,uploadmenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragemnt_my,container,false);



        init();
        return rootView;
    }

    private void  init(){
        imagephoto = rootView.findViewById(R.id.imagephoto);
        username = rootView.findViewById(R.id.username);

        if (MyApplication.sUserInfo.getPhotoimage() != null){
            Bitmap bitmap = BitmapFactory.decodeFile(MyApplication.sUserInfo.getPhotoimage());
            imagephoto.setImageBitmap(bitmap);
        }

        username.setText(MyApplication.sUserInfo.getNickname());

        btn_login = rootView.findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 MyApplication.sUserInfo =null;
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);

            }
        });

        customTextView = rootView.findViewById(R.id.customTextView);
        customTextView.setOnTextViewClickListener(new CustomTextView.OnTextViewClickListener(){

            @Override
            public void OnTextViewClick() {

                Intent  intent = new Intent(getActivity(), UpdateUserinfoActivity.class);
                startActivity(intent);

            }
        });
        uploadmenu = rootView.findViewById(R.id.uploadmenu);
        uploadmenu.setOnTextViewClickListener(new CustomTextView.OnTextViewClickListener(){

            @Override
            public void OnTextViewClick() {

                Intent  intent1 = new Intent(getActivity(), MenuUpLoadActivity.class);
                startActivity(intent1);

            }
        });

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d("执行","执行了");
         init();
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }
}
