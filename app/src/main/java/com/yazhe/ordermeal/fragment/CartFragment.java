package com.yazhe.ordermeal.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.adapter.CartAdapter;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.dao.MenuDao;
import com.yazhe.ordermeal.pojo.CartInfo;
import com.yazhe.ordermeal.pojo.Menu;
import com.yazhe.ordermeal.pojo.OrderDetail;
import com.yazhe.ordermeal.pojo.Orders;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CartFragment extends Fragment implements CartAdapter.MyCallBack {

    private View rootView;
    private MenuDao mMenuDao;
    private ListView cardlistview;
    private List<CartInfo> mCartInfoList;

    private CartAdapter mCartAdapter;

    private CheckBox allcheckbox;

    private TextView countprice;

    private Button payorder;

    //选中的集合
    private      List<CartInfo> ids;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(getActivity(),"ordermeal.db",null, MyApplication.dataBaseVersion);
        dictionSQLiteOpenHelper.getWritableDatabase().close();

        mMenuDao = new MenuDao(dictionSQLiteOpenHelper);
        rootView=inflater.inflate(R.layout.fragemnt_cart,container,false);
        initData();
        init();

        return rootView;
    }

     private void initData() {
        mCartInfoList =  mMenuDao.selectCar(MyApplication.sUserInfo.getUsername());



    }

    private void init() {
        countprice = rootView.findViewById(R.id.countprice);
        allcheckbox = rootView.findViewById(R.id.allcheckbox);
        cardlistview = rootView.findViewById(R.id.cardlistview);
        payorder = rootView.findViewById(R.id.payorder);
        mCartAdapter = new CartAdapter(getContext(),mCartInfoList,this);
        cardlistview.setAdapter(mCartAdapter);
        allcheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                    btnSelectAllList();
                }else{

                    btnNoList();
                }
            }
        });
        payorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  if (ids!=null&&ids.size()>0){
                    //下单
                    String orderid = MyApplication.sUserInfo.getUsername() + new Date().getTime();
                    int[] menuids = new int[ids.size()];
                    for (int i = 0; i < ids.size(); i++) {
                        OrderDetail orderDetail = new OrderDetail();
                        orderDetail.setMenuid(ids.get(i).getMenuid());
                        orderDetail.setOrderid(orderid);
                        orderDetail.setPrice(ids.get(i).getPrice());
                        orderDetail.setMenunum(ids.get(i).getMenunum());
                        menuids[i] = ids.get(i).getMenuid();
                        //订单详情
                        Long row = mMenuDao.insertorderdeatil(orderDetail);
                        if (row > 0) {

                        } else {
                            break;
                        }
                    }

                    //订单
                    Orders orders = new Orders();
                    orders.setOrderid(orderid);

                    orders.setUsername(MyApplication.sUserInfo.getUsername());
                    orders.setOrderprice(Double.parseDouble(countprice.getText().toString().trim()));
                    orders.setOrderdetailid(Arrays.toString(menuids));

                    Long rows = mMenuDao.insertorder(orders);
                    if (rows > 0) {

                        for (CartInfo cartInfo : ids) {
                            mMenuDao.deletecart(MyApplication.sUserInfo.getUsername(), cartInfo.getMenuid());
                        }
                        ids.clear();
                        mCartInfoList.clear();
                        allcheckbox.setChecked(false);
                        initData();
                        init();


                        TipDialog.show("下单成功", WaitDialog.TYPE.SUCCESS, 2000);
                        //删除cart的数据


                    } else {
                        TipDialog.show("下单失败", WaitDialog.TYPE.ERROR, 2000);
                    }
                }
            }
        });



    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        initData();
        init();

    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
        init();

    }
    /**
     * 全选
     *
     */
    public void btnSelectAllList() {
            double sumprice =0.0;
            for (int i = 0; i < mCartInfoList.size(); i++) {
                mCartInfoList.get(i).setChecked(true);
                sumprice +=  mCartInfoList.get(i).getMenunum()*mCartInfoList.get(i).getPrice();
            }
             ids = mCartInfoList;
             countprice.setText(sumprice+"");
            mCartAdapter.notifyDataSetChanged();

        }
    /**
     * 全不选
     *
     */
    public void btnNoList() {


            for (int i = 0; i < mCartInfoList.size(); i++) {
                mCartInfoList.get(i).setChecked(false);
            }
            if (ids==null){

            }else{
                ids.clear();
            }
        countprice.setText("0.0");
        mCartAdapter.notifyDataSetChanged();


    }

    /**
     * 获取选中数据
     *
     */
    public void btnOperateList(View view) {


             ids = new ArrayList<CartInfo>();


            double sumprice =0.0;
            for (int i = 0; i < mCartInfoList.size(); i++) {
                if (mCartInfoList.get(i).isChecked()) {
                    ids.add(mCartInfoList.get(i));
                    sumprice+=mCartInfoList.get(i).getPrice()*mCartInfoList.get(i).getMenunum();


                }
            }
            if (ids.size()==mCartInfoList.size()){
                allcheckbox.setChecked(true);
            }
            if (ids.size()==0){
                allcheckbox.setChecked(false);
            }
         countprice.setText(sumprice+"");

    }

    @Override
    public void onTtemWidgetClickListener(View view) {
             btnOperateList(view);
    }
}
