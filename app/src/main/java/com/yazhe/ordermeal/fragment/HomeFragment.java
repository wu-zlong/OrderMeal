package com.yazhe.ordermeal.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.yazhe.ordermeal.LoginActivity;
import com.yazhe.ordermeal.MainActivity;
import com.yazhe.ordermeal.MenuDetailActivity;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.adapter.HomeAdapter;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.dao.MenuDao;
import com.yazhe.ordermeal.dao.UserDao;
import com.yazhe.ordermeal.pojo.Cart;
import com.yazhe.ordermeal.pojo.Menu;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private RecyclerView menuRecycle;

    private View rootView;

    private HomeAdapter mHomeAdapter;

    private List<Menu>  mMenuList;

    private SwipeRefreshLayout refreshLayout;

    private MenuDao  mMenuDao;

    private int pageSize =10;
    private int pageNum =1;

    private Handler mHandler = new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {

            switch (msg.what){
                case 1:
                    refreshLayout.setRefreshing(false);

                    mHomeAdapter.getLoadMoreModule().setEnableLoadMore(true);
                    if(pageNum==1){
                        mHomeAdapter.setList(mMenuList);
                    }else{
                        mHomeAdapter.addData(mMenuList);
                    }
                    if(mMenuList.size()<pageSize){
                        mHomeAdapter.getLoadMoreModule().loadMoreEnd();

                }else{
                        mHomeAdapter.getLoadMoreModule().loadMoreComplete();
                }

                  pageNum++;

                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =inflater.inflate(R.layout.fragemnt_home,container,false);
        DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(getActivity(),"ordermeal.db",null, MyApplication.dataBaseVersion);
        dictionSQLiteOpenHelper.getWritableDatabase().close();

        mMenuDao = new MenuDao(dictionSQLiteOpenHelper);


        refreshLayout = rootView.findViewById(R.id.refreshLayout);
        menuRecycle = rootView.findViewById(R.id.menurecycle);
        menuRecycle.setLayoutManager(new GridLayoutManager(getActivity(),2));
        menuRecycle.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mHomeAdapter = new HomeAdapter(R.layout.item_home);
        mHomeAdapter.setList(mMenuList);

        menuRecycle.setAdapter(mHomeAdapter);
        initRefreshLayout();
        initLoadMore();
        addLinsteren();
         initData();

        return rootView;
    }

    private void initData(){
         //查询菜单

         mMenuList =   mMenuDao.selectPage(pageSize,(pageNum-1)*pageSize);

        mHomeAdapter.setList(mMenuList);
        mHomeAdapter.notifyDataSetChanged();


    }
    /**
     * 下拉刷新
     */
    private void initRefreshLayout(){
        refreshLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mHomeAdapter.getLoadMoreModule().setEnableLoadMore(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mMenuList =   mMenuDao.selectPage(pageSize,(pageNum-1)*pageSize);
                        Message message = new Message();
                        message.what =1;
                        mHandler.sendMessage(message);
                    }
                }).start();


            }
        });
    }

    /**
     * 上拉加载
     */
    private void initLoadMore(){
        mHomeAdapter.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                refreshLayout.setRefreshing(false);
                mHomeAdapter.getLoadMoreModule().setEnableLoadMore(true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mMenuList =   mMenuDao.selectPage(pageSize,(pageNum-1)*pageSize);
                        Message message = new Message();
                        message.what =1;
                        mHandler.sendMessage(message);
                    }
                }).start();

            }
        });
        mHomeAdapter.getLoadMoreModule().setAutoLoadMore(true);
        mHomeAdapter.getLoadMoreModule().setEnableLoadMoreIfNotFullPage(false);
    }
    private void addLinsteren() {

        mHomeAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(@NonNull @NotNull BaseQuickAdapter adapter, @NonNull @NotNull View view, int position) {

                WaitDialog.show("请等待...",0.1f);
                //加入购物车
                Menu  menu = (Menu) adapter.getData().get(position);
                Cart cart = new Cart();
                cart.setUsername(MyApplication.sUserInfo.getUsername());
                cart.setMenuid(menu.getId());
                cart.setMenunum(1);

               Cart cart1 =   mMenuDao.selectByuserNameAndMenuId(MyApplication.sUserInfo.getUsername(),menu.getId());


                long row =0L;
               if (cart1!=null){
                   Log.d("修改","修改");
                   cart.setMenunum(cart1.getMenunum()+1);
                   row =   mMenuDao.updatecartnum(cart);

               }else{
                    row = mMenuDao.insertCart(cart);
                   Log.d("添加","添加");
               }


                if (row>0) {
                    WaitDialog.dismiss();
                    TipDialog.show("添加成功", WaitDialog.TYPE.SUCCESS,2000);

                }else{
                    WaitDialog.dismiss();
                    TipDialog.show("添加失败", WaitDialog.TYPE.ERROR,2000);
                }



            }
        });

        mHomeAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull @NotNull BaseQuickAdapter<?, ?> adapter, @NonNull @NotNull View view, int position) {
                 Menu  menu = (Menu) adapter.getData().get(position);
                Intent intent = new Intent(getActivity(), MenuDetailActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("menu",menu);
                intent.putExtras(mBundle);



                startActivity(intent);

            }
        });


    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        initData();


    }

    @Override
    public void onResume() {
        super.onResume();
        initData();


    }
}
