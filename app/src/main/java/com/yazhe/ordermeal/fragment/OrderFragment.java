package com.yazhe.ordermeal.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.yazhe.ordermeal.R;
import com.yazhe.ordermeal.adapter.OrderAdapter;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.dao.MenuDao;
import com.yazhe.ordermeal.pojo.OrderBase;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class OrderFragment extends Fragment {

    private RecyclerView orderrecycle;

    private View rootView;

    private MenuDao mMenuDao;
    private SwipeRefreshLayout refreshLayout;

    private OrderAdapter  mOrderAdapter = new OrderAdapter();
    private int pageSize =10;
    private int pageNum =1;

    private List<OrderBase> mOrderBaseList;

    private Handler mHandler = new Handler(Looper.myLooper()){

        @Override
        public void handleMessage(@NonNull Message msg) {

            switch (msg.what){
                case 1:
                    refreshLayout.setRefreshing(false);

                    mOrderAdapter.getLoadMoreModule().setEnableLoadMore(true);
                    if(pageNum==1){
                        mOrderAdapter.setList(mOrderBaseList);
                    }else{
                        mOrderAdapter.addData(mOrderBaseList);
                    }
                    if(mOrderBaseList.size()<pageSize){
                        mOrderAdapter.getLoadMoreModule().loadMoreEnd();

                    }else{
                        mOrderAdapter.getLoadMoreModule().loadMoreComplete();
                    }

                    pageNum++;

                    break;
            }
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView =inflater.inflate(R.layout.fragemnt_order,container,false);
        DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(getActivity(),"ordermeal.db",null, MyApplication.dataBaseVersion);
        dictionSQLiteOpenHelper.getWritableDatabase().close();

        mMenuDao = new MenuDao(dictionSQLiteOpenHelper);

        init();
        initdata();
        return rootView;
    }

    private void init() {
         orderrecycle = rootView.findViewById(R.id.orderrecycle);
          refreshLayout = rootView.findViewById(R.id.refreshLayout);
         orderrecycle.setLayoutManager(new LinearLayoutManager(getContext()));
         orderrecycle.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
         orderrecycle.setAdapter(mOrderAdapter);

         mOrderAdapter.setList(mOrderBaseList);
         initRefreshLayout();
         initLoadMore();


    }

    private void initdata(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                mOrderBaseList = mMenuDao.selectPageOrder(pageSize,pageNum,MyApplication.sUserInfo.getUsername());
                Message message = new Message();
                message.what =1;
                mHandler.sendMessage(message);
            }
        }).start();



    }
    /**
     * 下拉刷新
     */
    private void initRefreshLayout(){
        refreshLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mOrderAdapter.getLoadMoreModule().setEnableLoadMore(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mOrderBaseList = mMenuDao.selectPageOrder(pageSize,pageNum,MyApplication.sUserInfo.getUsername());
                        Message message = new Message();
                        message.what =1;
                        mHandler.sendMessage(message);
                    }
                }).start();


            }
        });
    }

    /**
     * 上拉加载
     */
    private void initLoadMore(){
        mOrderAdapter.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                refreshLayout.setRefreshing(false);
                mOrderAdapter.getLoadMoreModule().setEnableLoadMore(true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mOrderBaseList = mMenuDao.selectPageOrder(pageSize,pageNum,MyApplication.sUserInfo.getUsername());
                        Message message = new Message();
                        message.what =1;
                        mHandler.sendMessage(message);
                    }
                }).start();

            }
        });
        mOrderAdapter.getLoadMoreModule().setAutoLoadMore(true);
        mOrderAdapter.getLoadMoreModule().setEnableLoadMoreIfNotFullPage(false);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);


        initdata();

    }

    @Override
    public void onResume() {
        super.onResume();

        initdata();

    }
}
