package com.yazhe.ordermeal.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.yazhe.ordermeal.pojo.Cart;
import com.yazhe.ordermeal.pojo.CartInfo;
import com.yazhe.ordermeal.pojo.Comment;
import com.yazhe.ordermeal.pojo.CommentList;
import com.yazhe.ordermeal.pojo.Menu;
import com.yazhe.ordermeal.pojo.OrderBase;
import com.yazhe.ordermeal.pojo.OrderBaseDetail;
import com.yazhe.ordermeal.pojo.OrderDetail;
import com.yazhe.ordermeal.pojo.Orders;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class MenuDao {

    private DictionSQLiteOpenHelper mDBhelper;

    public MenuDao(DictionSQLiteOpenHelper mDictionSQLiteOpenHelper ) {
        this.mDBhelper = mDictionSQLiteOpenHelper;
    }

    /**
     * 分页查询
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public List<Menu> selectPage(int pageSize,int pageIndex){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select * from  menu order by id limit ? offset ?",new String[]{pageSize+"",pageIndex+""});
        List<Menu> list = new ArrayList<Menu>();
        while (cursor.moveToNext()){

            @SuppressLint("Range") int id =cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String menuname =cursor.getString(cursor.getColumnIndex("menuname"));
            @SuppressLint("Range") double price =cursor.getDouble(cursor.getColumnIndex("price"));
            @SuppressLint("Range") String menuInfo =cursor.getString(cursor.getColumnIndex("menuInfo"));
            @SuppressLint("Range") String imags =cursor.getString(cursor.getColumnIndex("imags"));

            Menu menu = new Menu(id,menuname,price,menuInfo,imags);
            list.add(menu);

        }

        Log.d("查询的数据",list.size()+"");
        cursor.close();

        return   list;
    }
    /**
     * 根据用户名与menuid查询是否再购物车存在
     */

    public  Cart selectByuserNameAndMenuId(String username,int menuid){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select * from cart where username=? and menuid=?",new String[]{username,menuid+""});

        Cart cart =null;
        while (cursor.moveToNext()){

            @SuppressLint("Range") int id =cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String usernames =cursor.getString(cursor.getColumnIndex("username"));

            @SuppressLint("Range")  int menuids =cursor.getInt(cursor.getColumnIndex("menuid"));
            @SuppressLint("Range")  int menunum =cursor.getInt(cursor.getColumnIndex("menunum"));



            cart = new Cart();
            cart.setId(id);
            cart.setMenuid(menuids);
            cart.setMenunum(menunum);
            cart.setUsername(username);
            Log.d("查询的cart",cart.toString());

        }
        cursor.close();
        return  cart ;

    }

    /**
     * 添加购物车
     * @param cart
     * @return
     */
    public Long insertCart(Cart cart){


        SQLiteDatabase db = mDBhelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("username",cart.getUsername());
        values.put("menuid",cart.getMenuid());
        values.put("menunum",cart.getMenunum());

        return   db.insert("cart",null,values);

    }

    /**
     * 修改
     * @param cart
     * @return
     */
    public int updatecartnum(Cart cart){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("menunum",cart.getMenunum());
        return   db.update("cart",values,"username=? and menuid=?",new String[]{cart.getUsername(),cart.getMenuid()+""});
    }



    /**
     * 查询用户的购物车
     * @param username
     * @return
     */
    public List<CartInfo> selectCar(String username){

        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select cart.id as id,cart.menunum as menunum,menu.menuname as menuname,menu.price as price,menu.imags as imags,cart.menuid as menuid from  cart inner join menu on cart.menuid =menu.id and cart.username=?",new String[]{username});
        List<CartInfo> list = new ArrayList<CartInfo>();
        while (cursor.moveToNext()){

            @SuppressLint("Range") int id =cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String menuname =cursor.getString(cursor.getColumnIndex("menuname"));
            @SuppressLint("Range") double price =cursor.getDouble(cursor.getColumnIndex("price"));
            @SuppressLint("Range")  int menunum =cursor.getInt(cursor.getColumnIndex("menunum"));
            @SuppressLint("Range") String imags =cursor.getString(cursor.getColumnIndex("imags"));
            @SuppressLint("Range")  int menuid =cursor.getInt(cursor.getColumnIndex("menuid"));
             CartInfo cartInfo = new CartInfo(id,menuname,price,menunum,imags,menuid);
             list.add(cartInfo);

        }

        Log.d("查询的数据",list.size()+"");
        cursor.close();

        return   list;



    }
    /**
     * 删除购物车已经下单的
     */

    public int deletecart(String username,int menuid){

        SQLiteDatabase db = mDBhelper.getWritableDatabase();


        return db.delete("cart","username=? and menuid=?",new String[]{username,menuid+""});


    }

    /**
     * 添加订单
     * @param orders
     * @return
     */
    public Long insertorder(Orders orders){
        SQLiteDatabase db = mDBhelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("username",orders.getUsername());
        values.put("orderid",orders.getOrderid());
        values.put("orderdetailid",orders.getOrderdetailid());
        values.put("orderprice",orders.getOrderprice());
        values.put("ordercreate", String.valueOf(new Date(new java.util.Date().getTime())));

        return   db.insert("orders",null,values);
    }

    /**
     * 添加订单详情
     * @param orderDetail
     * @return
     */
    public Long insertorderdeatil(OrderDetail orderDetail){
        SQLiteDatabase db = mDBhelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("menuid",orderDetail.getMenuid());
        values.put("orderid",orderDetail.getOrderid());
        values.put("menunum",orderDetail.getMenunum());
        values.put("price",orderDetail.getPrice()*orderDetail.getMenunum());

        return   db.insert("ordersdetail",null,values);
    }

    /**
     * 查询订单
     */
    /**
     * 分页查询
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public List<OrderBase> selectPageOrder(int pageSize,int pageIndex,String username){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        List<OrderBase> orderBaseList = new ArrayList<>();
         Cursor  cursor = db.rawQuery("select * from orders where username=?  order by id limit ? offset ?",new String[]{username,pageSize+"",pageIndex+""});
        while (cursor.moveToNext()){
            @SuppressLint("Range") int id =cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String ordetailid =cursor.getString(cursor.getColumnIndex("orderdetailid"));
            @SuppressLint("Range") String orderid =cursor.getString(cursor.getColumnIndex("orderid"));
            @SuppressLint("Range") double orderprice =cursor.getDouble(cursor.getColumnIndex("orderprice"));
            @SuppressLint("Range")  String ordercreate =cursor.getString(cursor.getColumnIndex("orderprice"));
           OrderBase orderBase = new OrderBase();
           orderBase.setId(id);
           orderBase.setOrdercreate(ordercreate);
           orderBase.setOrderid(orderid);
           orderBase.setOrderprice(orderprice);
            orderBaseList.add(orderBase);
        }
       cursor.close();
        SQLiteDatabase db1 = mDBhelper.getReadableDatabase();

         for (OrderBase orderBase:orderBaseList){
              List<BaseNode> orderBaseDetailList = new ArrayList<>();
             Cursor cursor1 = db1.rawQuery("select ordersdetail.id as id,menu.imags as imags,menu.menuname as menuname,ordersdetail.price as price,ordersdetail.menunum as menunum from ordersdetail,menu where orderid=? and ordersdetail.menuid = menu.id ",new String[]{orderBase.getOrderid()});
             while (cursor1.moveToNext()){

                 @SuppressLint("Range") int id =cursor1.getInt(cursor1.getColumnIndex("id"));
                 @SuppressLint("Range") String menuname =cursor1.getString(cursor1.getColumnIndex("menuname"));
                 @SuppressLint("Range") double price =cursor1.getDouble(cursor1.getColumnIndex("price"));
                 @SuppressLint("Range") int menunum =cursor1.getInt(cursor1.getColumnIndex("menunum"));
                 @SuppressLint("Range") String menuimage =cursor1.getString(cursor1.getColumnIndex("imags"));
                   OrderBaseDetail orderBaseDetail = new OrderBaseDetail();
                   orderBaseDetail.setId(id);
                   orderBaseDetail.setPrice(price);
                   orderBaseDetail.setMenunum(menunum);
                   orderBaseDetail.setMenuname(menuname);
                   orderBaseDetail.setMenuimage(menuimage);
                  orderBaseDetailList.add(orderBaseDetail);
             }
             cursor1.close();
             orderBase.setOrderBaseDetailList(orderBaseDetailList);

         }







         Log.d("查询的订单数据",orderBaseList.size()+"");


         for (OrderBase orderBase:orderBaseList){
             Log.d("一级",orderBase.toString());
         }



//        cursor.close();

        return   orderBaseList;
    }


    public Long insertMenu(Menu menu){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("menuname",menu.getMenuname());
        values.put("price",menu.getPrice());
        values.put("menuInfo",menu.getMenuInfo());
        values.put("imags",menu.getImags());
       return db.insert("menu",null,values);

    }

    /**
     * 添加评价
     * @param comment
     * @return
     */
    public Long  insertComment(Comment comment){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("username",comment.getUsername());
        values.put("commentcontent",comment.getCommentcontent());
        values.put("menuid",comment.getMenuid());
        values.put("commentdate", String.valueOf(new Date(new java.util.Date().getTime())));

        return db.insert("comment",null,values);
    }

    /**
     * 分页查询评论
     */

    /**
     * 分页查询
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public List<CommentList> selectCommentPage(int pageSize,int pageIndex,int menuid){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select userinfo.username as username,userinfo.nickname as nickname,userinfo.photoimage as photoimage,commentcontent,commentdate from  comment,userinfo where comment.username = userinfo.username and  menuid=? order by commentdate DESC limit ? offset ?",new String[]{menuid+"",pageSize+"",pageIndex+""});
        List<CommentList> list = new ArrayList<CommentList>();
        while (cursor.moveToNext()){


            @SuppressLint("Range") String username =cursor.getString(cursor.getColumnIndex("username"));

            @SuppressLint("Range") String nickname =cursor.getString(cursor.getColumnIndex("nickname"));
            @SuppressLint("Range") String photoimage =cursor.getString(cursor.getColumnIndex("photoimage"));
            @SuppressLint("Range") String commentcontent =cursor.getString(cursor.getColumnIndex("commentcontent"));
            @SuppressLint("Range") String commentdate =cursor.getString(cursor.getColumnIndex("commentdate"));

                CommentList  commentList = new CommentList();

                commentList.setUsername(username);
                commentList.setCommentcontent(commentcontent);
                commentList.setNickname(nickname);
                commentList.setPhotoimage(photoimage);
                commentList.setCommentdate(commentdate);

            list.add(commentList);

        }

        Log.d("查询的数据",list.size()+"");
        cursor.close();

        return   list;
    }




}
