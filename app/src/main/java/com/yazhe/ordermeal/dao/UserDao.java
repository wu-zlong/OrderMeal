package com.yazhe.ordermeal.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.yazhe.ordermeal.pojo.UserInfo;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;

public  class UserDao {
    private DictionSQLiteOpenHelper mDBhelper;

    public UserDao(DictionSQLiteOpenHelper mDictionSQLiteOpenHelper ) {
        this.mDBhelper = mDictionSQLiteOpenHelper;
    }

    /**
     * 注册用户
     * @return
     */
    public Long  registerUser(UserInfo mUserInfo){

        SQLiteDatabase db = mDBhelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("username",mUserInfo.getUsername());
        values.put("pwd",mUserInfo.getPwd());
        values.put("nickname",mUserInfo.getNickname());
        values.put("tel",mUserInfo.getTel());
        values.put("photoimage",mUserInfo.getPhotoimage());
      return   db.insert("userinfo",null,values);
    }

    /**
     * 登录
     */
    public UserInfo login(UserInfo userInfo){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();

        Cursor cursor =db.rawQuery("select * from userinfo where username=? and pwd =?",new String[]{userInfo.getUsername(),userInfo.getPwd()});
        UserInfo  userInfo1 =null;
        while (cursor.moveToNext()){

            @SuppressLint("Range") int id =cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String username =cursor.getString(cursor.getColumnIndex("username"));
            @SuppressLint("Range") String pwd = cursor.getString(cursor.getColumnIndex("pwd"));
            @SuppressLint("Range") String nickname = cursor.getString(cursor.getColumnIndex("nickname"));
            @SuppressLint("Range") String tel = cursor.getString(cursor.getColumnIndex("tel"));
            @SuppressLint("Range") String photoimage = cursor.getString(cursor.getColumnIndex("photoimage"));
            userInfo1 = new UserInfo();
            userInfo1.setId(id);
            userInfo1.setUsername(username);
            userInfo1.setNickname(nickname);
            userInfo1.setPwd(pwd);
            userInfo1.setTel(tel);
            userInfo1.setPhotoimage(photoimage);
        }
        cursor.close();
        db.close();


        return userInfo1;
    }
    /**
     * 通过用户名查询
     */
    public UserInfo getUserNameByName(String account){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();

        Cursor cursor =db.rawQuery("select * from userinfo where username=?",new String[]{account});
        UserInfo  userInfo1 =null;
        while (cursor.moveToNext()){

            @SuppressLint("Range") int id =cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") String username =cursor.getString(cursor.getColumnIndex("username"));
            @SuppressLint("Range") String pwd = cursor.getString(cursor.getColumnIndex("pwd"));
            @SuppressLint("Range") String nickname = cursor.getString(cursor.getColumnIndex("nickname"));
            @SuppressLint("Range") String tel = cursor.getString(cursor.getColumnIndex("tel"));
            @SuppressLint("Range") String photoimage = cursor.getString(cursor.getColumnIndex("photoimage"));
            userInfo1 = new UserInfo();
            userInfo1.setId(id);
            userInfo1.setUsername(username);
            userInfo1.setNickname(nickname);
            userInfo1.setPwd(pwd);
            userInfo1.setTel(tel);
            userInfo1.setPhotoimage(photoimage);
        }
        cursor.close();
        db.close();


        return userInfo1;
    }
    /**
     * 修改信息

     * @return
     */
    public int updateuserinfo(UserInfo userInfo){
        SQLiteDatabase db = mDBhelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("photoimage",userInfo.getPhotoimage());
        values.put("nickname",userInfo.getNickname());
        values.put("pwd",userInfo.getPwd());
        values.put("tel",userInfo.getTel());


        return   db.update("userinfo",values,"username=?",new String[]{userInfo.getUsername()});
    }




}
