package com.yazhe.ordermeal.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.yazhe.ordermeal.pojo.Menu;


/**
 * 数据库
 */
public class DictionSQLiteOpenHelper extends SQLiteOpenHelper {


    public DictionSQLiteOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DictionSQLiteOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public DictionSQLiteOpenHelper(@Nullable Context context, @Nullable String name, int version, @NonNull SQLiteDatabase.OpenParams openParams) {
        super(context, name, version, openParams);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

              /**
         * 创建用户表
          */
        String CREATE_COMPNAY_TABLE =  "CREATE TABLE " + "userinfo" + "("
                + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "username" + " TEXT,"
                + "pwd" + " TEXT," + "nickname" + " TEXT,"+ "photoimage" + " TEXT," + "tel" + " TEXT" +")";

        sqLiteDatabase.execSQL(CREATE_COMPNAY_TABLE);
        /**
         * 菜单表
         */
        String MENU_CREATE_COMPNAY_TABLE =  "CREATE TABLE " + "menu" + "("
                + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "menuname" + " TEXT,"
                + "price" + " DOUBLE," + "menuInfo" + " TEXT," + "imags" + " TEXT" +")";

        sqLiteDatabase.execSQL(MENU_CREATE_COMPNAY_TABLE);


        /**
         * 购物车
         */

        String CART_CREATE_COMPNAY_TABLE =  "CREATE TABLE " + "cart" + "("
                + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "username" + " TEXT,"
                + "menuid" + " INT," + "menunum" + " INT " +")";

        sqLiteDatabase.execSQL(CART_CREATE_COMPNAY_TABLE);
        /**
         * 订单表
         */

        String ORDER_CREATE_COMPNAY_TABLE =  "CREATE TABLE " + "orders" + "("
                + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "username" + " TEXT,"
                + "orderid" + " TEXT," + "orderprice" + " DOUBLE,"  + "ordercreate" + " DATETIME,"  + "orderdetailid" + " TEXT"+")";

        sqLiteDatabase.execSQL(ORDER_CREATE_COMPNAY_TABLE);
        /**
         * 订单表
         */

        String ORDER_DETAIL_CREATE_COMPNAY_TABLE =  "CREATE TABLE " + "ordersdetail" + "("
                + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "menuid" + " INT,"
                + "orderid" + " TEXT," + "menunum" + " INT,"  + "price" + " DOUBLE"+")";

        sqLiteDatabase.execSQL(ORDER_DETAIL_CREATE_COMPNAY_TABLE);
        /**
         * 评价表
         */
        String COMMENT_CREATE_COMPNAY_TABLE =  "CREATE TABLE " + "comment" + "("
                + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "username" + " TEXT,"
                + "commentcontent" + " TEXT," + "menuid" + " INT," + "commentdate" + " DATETIME"+")";

        sqLiteDatabase.execSQL(COMMENT_CREATE_COMPNAY_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {



    }
}
