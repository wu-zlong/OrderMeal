package com.yazhe.ordermeal;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.dao.MenuDao;
import com.yazhe.ordermeal.dao.UserDao;
import com.yazhe.ordermeal.pojo.Menu;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;
import com.yazhe.ordermeal.view.CustomTextView;

public class MenuUpLoadActivity extends AppCompatActivity {
    private ImageView menuimage;

    private MenuDao mMenuDao;

    private EditText menuname,menuprice,synopsis;

    private Button btn_upload;

    private  String imagephotoPath;

    private CustomTextView titlebar;

    @Override
    protected void onCreate(@Nullable  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_upload);
        DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(this,"ordermeal.db",null, MyApplication.dataBaseVersion);
        dictionSQLiteOpenHelper.getWritableDatabase().close();

        mMenuDao = new MenuDao(dictionSQLiteOpenHelper);
        init();
    }


    private void  init(){
        menuimage  =findViewById(R.id.menuimg);
        menuname =findViewById(R.id.menuname);
        menuprice =findViewById(R.id.menuprice);
        synopsis =findViewById(R.id.synopsis);

        btn_upload =findViewById(R.id.btn_upload);

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  name =  menuname.getText().toString().trim();
                if (TextUtils.isEmpty(name)){
                    TipDialog.show("菜名不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }
                String  price =  menuprice.getText().toString().trim();
                if (TextUtils.isEmpty(price)){
                    TipDialog.show("价格不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }

                //开始上传

                WaitDialog.show("请等待...",0.1f);
               String menuinfo = synopsis.getText().toString().trim();
                Menu  menu = new Menu();
                menu.setMenuInfo(menuinfo);
                menu.setMenuname(name);
                menu.setPrice(Double.parseDouble(price));
                menu.setImags(imagephotoPath);
               Long row =  mMenuDao.insertMenu(menu);
                if (row>0){
                    TipDialog.show("上传成功", WaitDialog.TYPE.SUCCESS,2000);
                }else{
                    TipDialog.show("上传失败", WaitDialog.TYPE.ERROR,2000);
                }



            }
        });

        menuimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAlbum();
            }
        });

        titlebar =findViewById(R.id.titlebar);
        titlebar.setOnTextViewClickListener( new CustomTextView.OnTextViewClickListener(){
            @Override
            public void OnLeftImgClick() {
                finish();
            }
        });
    }
    //启动相册的方法
    private void openAlbum(){
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent,2);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            //判断安卓版本
            if (resultCode == RESULT_OK && data != null) {
                if (Build.VERSION.SDK_INT >= 19)
                    handImage(data);
                else handImageLow(data);
            }
        }
    }

    //安卓版本大于4.4的处理方法
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handImage(Intent data){
        String path =null;
        Uri uri = data.getData();
        //根据不同的uri进行不同的解析
        if (DocumentsContract.isDocumentUri(this,uri)){
            String docId = DocumentsContract.getDocumentId(uri);
            if ("com.android.providers.media.documents".equals(uri.getAuthority())){
                String id = docId.split(":")[1];
                String selection = MediaStore.Images.Media._ID+"="+id;
                path = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,selection);
            }else if("com.android.providers.downloads.documents".equals(uri.getAuthority())){
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),Long.valueOf(docId));
                path = getImagePath(contentUri,null);
            }
        }else if ("content".equalsIgnoreCase(uri.getScheme())){
            path = getImagePath(uri,null);
        }else if ("file".equalsIgnoreCase(uri.getScheme())){
            path = uri.getPath();
        }
        //展示图片
        displayImage(path);
    }


    //安卓小于4.4的处理方法
    private void handImageLow(Intent data){
        Uri uri = data.getData();
        String path = getImagePath(uri,null);
        displayImage(path);
    }

    //content类型的uri获取图片路径的方法
    @SuppressLint("Range")
    private String getImagePath(Uri uri, String selection) {
        String path = null;
        Cursor cursor = getContentResolver().query(uri,null,selection,null,null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

    //根据路径展示图片的方法
    private void displayImage(String imagePath){
        imagephotoPath = imagePath;
        if (imagePath != null){
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            menuimage.setImageBitmap(bitmap);
        }else{
            Toast.makeText(this,"fail to set image", Toast.LENGTH_SHORT).show();
        }
    }
}
