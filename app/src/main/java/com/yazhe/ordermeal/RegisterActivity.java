package com.yazhe.ordermeal;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.dao.UserDao;
import com.yazhe.ordermeal.pojo.UserInfo;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;

/**
 * 注册
 */
public class RegisterActivity extends AppCompatActivity {
    private UserDao mUserDao;
    private ImageView  imagephoto;

    private EditText nickname,username,password,telphone;

    private Button btn_register;

   private  String imagephotoPath;

    @Override
    protected void onCreate(@Nullable  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(this,"ordermeal.db",null, MyApplication.dataBaseVersion);
        dictionSQLiteOpenHelper.getWritableDatabase().close();

        mUserDao = new UserDao(dictionSQLiteOpenHelper);
        init();
    }


    private void init(){
        imagephoto  =findViewById(R.id.imagephoto);
        nickname =findViewById(R.id.nickname);
        username =findViewById(R.id.username);
        password =findViewById(R.id.password);
        telphone =findViewById(R.id.telphone);
        btn_register =findViewById(R.id.btn_register);


        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //注册
                String  nicknames =  nickname.getText().toString().trim();
                if (TextUtils.isEmpty(nicknames)){
                    TipDialog.show("昵称不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }
                String  userName =  username.getText().toString().trim();
                if (TextUtils.isEmpty(userName)){
                    TipDialog.show("用户名不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }
                String pwd =  password.getText().toString().trim();
                if (TextUtils.isEmpty(pwd)){
                    TipDialog.show("密码不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }
                String tel =  telphone.getText().toString().trim();
                if (TextUtils.isEmpty(pwd)){
                    TipDialog.show("电话号码不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }
                WaitDialog.show("请等待...",0.1f);
                UserInfo  userInfo =new UserInfo();
                userInfo.setUsername(userName);
                userInfo.setPwd(pwd);
                userInfo.setPhotoimage(imagephotoPath);
                userInfo.setTel(tel);
                userInfo.setNickname(nicknames);

                //注册
              Long  row =  mUserDao.registerUser(userInfo);

                if (row>0) {
                    WaitDialog.dismiss();

                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                }else{
                    WaitDialog.dismiss();
                    TipDialog.show("注册失败", WaitDialog.TYPE.ERROR,2000);
                }

            }
        });
        imagephoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAlbum();
            }
        });
    }
    //启动相册的方法
    private void openAlbum(){
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent,2);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            //判断安卓版本
            if (resultCode == RESULT_OK && data != null) {
                if (Build.VERSION.SDK_INT >= 19)
                    handImage(data);
                else handImageLow(data);
            }
        }
    }

    //安卓版本大于4.4的处理方法
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handImage(Intent data){
        String path =null;
        Uri uri = data.getData();
        //根据不同的uri进行不同的解析
        if (DocumentsContract.isDocumentUri(this,uri)){
            String docId = DocumentsContract.getDocumentId(uri);
            if ("com.android.providers.media.documents".equals(uri.getAuthority())){
                String id = docId.split(":")[1];
                String selection = MediaStore.Images.Media._ID+"="+id;
                path = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,selection);
            }else if("com.android.providers.downloads.documents".equals(uri.getAuthority())){
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),Long.valueOf(docId));
                path = getImagePath(contentUri,null);
            }
        }else if ("content".equalsIgnoreCase(uri.getScheme())){
            path = getImagePath(uri,null);
        }else if ("file".equalsIgnoreCase(uri.getScheme())){
            path = uri.getPath();
        }
        //展示图片
        displayImage(path);
    }


    //安卓小于4.4的处理方法
    private void handImageLow(Intent data){
        Uri uri = data.getData();
        String path = getImagePath(uri,null);
        displayImage(path);
    }

    //content类型的uri获取图片路径的方法
    @SuppressLint("Range")
    private String getImagePath(Uri uri, String selection) {
        String path = null;
        Cursor cursor = getContentResolver().query(uri,null,selection,null,null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

    //根据路径展示图片的方法
    private void displayImage(String imagePath){
        imagephotoPath = imagePath;
        if (imagePath != null){
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            imagephoto.setImageBitmap(bitmap);
        }else{
            Toast.makeText(this,"fail to set image", Toast.LENGTH_SHORT).show();
        }
    }
}
