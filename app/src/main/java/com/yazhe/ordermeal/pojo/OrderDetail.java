package com.yazhe.ordermeal.pojo;

public class OrderDetail {

    private int id;

    private int menuid;

    private  String orderid;

    private int menunum;

    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public int getMenunum() {
        return menunum;
    }

    public void setMenunum(int menunum) {
        this.menunum = menunum;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
