package com.yazhe.ordermeal.pojo;

import com.chad.library.adapter.base.entity.node.BaseExpandNode;
import com.chad.library.adapter.base.entity.node.BaseNode;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public class OrderBase extends BaseExpandNode {

    private int id;

    private String orderid;

    private double orderprice;

    private String ordercreate;


    private List<BaseNode> mOrderBaseDetailList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrdercreate() {
        return ordercreate;
    }

    public void setOrdercreate(String ordercreate) {
        this.ordercreate = ordercreate;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public double getOrderprice() {
        return orderprice;
    }

    public void setOrderprice(double orderprice) {
        this.orderprice = orderprice;
    }

    public List<BaseNode> getOrderBaseDetailList() {
        return mOrderBaseDetailList;
    }

    public void setOrderBaseDetailList(List<BaseNode> orderBaseDetailList) {
        mOrderBaseDetailList = orderBaseDetailList;
    }

    public OrderBase() {
        setExpanded(false);
    }

    @Override
    public String toString() {
        return "OrderBase{" +
                "id=" + id +
                ", orderid='" + orderid + '\'' +
                ", orderprice=" + orderprice +
                ", mOrderBaseDetailList=" + mOrderBaseDetailList +
                '}';
    }

    @Nullable
    @Override
    public List<BaseNode> getChildNode() {


        return mOrderBaseDetailList;
    }
}
