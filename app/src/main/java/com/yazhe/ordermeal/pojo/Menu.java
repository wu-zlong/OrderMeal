package com.yazhe.ordermeal.pojo;

import java.io.Serializable;

public class Menu implements Serializable  {

    private int id;

    private String menuname;

    private double price;

    private String menuInfo;

    private String imags;

    public Menu() {
    }

    public Menu(int id, String menuname, double price, String menuInfo, String imags) {
        this.id = id;
        this.menuname = menuname;
        this.price = price;
        this.menuInfo = menuInfo;
        this.imags = imags;
    }

    public String getImags() {
        return imags;
    }

    public void setImags(String imags) {
        this.imags = imags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getMenuInfo() {
        return menuInfo;
    }

    public void setMenuInfo(String menuInfo) {
        this.menuInfo = menuInfo;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", menuname='" + menuname + '\'' +
                ", price=" + price +
                ", menuInfo='" + menuInfo + '\'' +
                ", imags='" + imags + '\'' +
                '}';
    }
}
