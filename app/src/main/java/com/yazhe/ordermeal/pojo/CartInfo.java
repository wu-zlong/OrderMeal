package com.yazhe.ordermeal.pojo;

public class CartInfo {

    private int id;
    private String menuimage;
    private String menuname;
    private  int menunum;
    private double price;
    private int menuid;
    private boolean isChecked;

    public CartInfo() {
    }

    public CartInfo(int id, String menuimage, String menuname, int menunum, double price) {
        this.id = id;
        this.menuimage = menuimage;
        this.menuname = menuname;
        this.menunum = menunum;
        this.price = price;
    }

    public CartInfo(int id, String menuname, double price, int menunum, String imags) {
        this.id = id;
        this.menuimage = menuimage;
        this.menuname = menuname;
        this.menunum = menunum;
        this.price = price;
    }

    public CartInfo(int id, String menuname, double price, int menunum, String imags, int menuid) {
        this.id = id;
        this.menuimage = menuimage;
        this.menuname = menuname;
        this.menunum = menunum;
        this.price = price;
        this.menuid =menuid;
    }

    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenuimage() {
        return menuimage;
    }

    public void setMenuimage(String menuimage) {
        this.menuimage = menuimage;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public int getMenunum() {
        return menunum;
    }

    public void setMenunum(int menunum) {
        this.menunum = menunum;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
