package com.yazhe.ordermeal.pojo;

import com.chad.library.adapter.base.entity.node.BaseExpandNode;
import com.chad.library.adapter.base.entity.node.BaseNode;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public class OrderBaseDetail extends BaseNode {

    private int  id;

    private String menuname;

    private double price;
    private int menunum;
    private String menuimage;

    public String getMenuimage() {
        return menuimage;
    }

    public void setMenuimage(String menuimage) {
        this.menuimage = menuimage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMenunum() {
        return menunum;
    }

    public void setMenunum(int menunum) {
        this.menunum = menunum;
    }

    @Override
    public String toString() {
        return "OrderBaseDetail{" +
                "id=" + id +
                ", menuname='" + menuname + '\'' +
                ", price=" + price +
                ", menunum=" + menunum +
                '}';
    }


    @Nullable
    @Override
    public List<BaseNode> getChildNode() {
        return null;
    }
}
