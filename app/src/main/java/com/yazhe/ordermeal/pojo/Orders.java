package com.yazhe.ordermeal.pojo;

import java.util.Date;

public class Orders {

    private int id;

    private String username;

    private String orderid;

    private double orderprice;

    private String orderdetailid;
    private String ordercreate;

    public String getOrdercreate() {
        return ordercreate;
    }

    public void setOrdercreate(String ordercreate) {
        this.ordercreate = ordercreate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public double getOrderprice() {
        return orderprice;
    }

    public void setOrderprice(double orderprice) {
        this.orderprice = orderprice;
    }

    public String getOrderdetailid() {
        return orderdetailid;
    }

    public void setOrderdetailid(String orderdetailid) {
        this.orderdetailid = orderdetailid;
    }
}
