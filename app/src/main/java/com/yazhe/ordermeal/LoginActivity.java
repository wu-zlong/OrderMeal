package com.yazhe.ordermeal;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.yazhe.ordermeal.app.MyApplication;
import com.yazhe.ordermeal.dao.UserDao;
import com.yazhe.ordermeal.pojo.UserInfo;
import com.yazhe.ordermeal.util.DictionSQLiteOpenHelper;
import com.yazhe.ordermeal.util.SharedPreferencesHelper;

import java.io.File;

/**
 * 登录
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private UserDao  mUserDao;
    private EditText username,pwd;

    private Button  login,register;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        if (isStoragePermissionGranted()) {

            SharedPreferencesHelper.init(getApplicationContext());

            int version = (int) SharedPreferencesHelper.getInstance().getData("version",MyApplication.dataBaseVersion);

            MyApplication.dataBaseVersion = version;
            DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(this,"ordermeal.db",null, MyApplication.dataBaseVersion);
            dictionSQLiteOpenHelper.getWritableDatabase().close();

            mUserDao = new UserDao(dictionSQLiteOpenHelper);
        }

    }


    private void init(){



        username = findViewById(R.id.username);
        pwd = findViewById(R.id.pwd);
        login = findViewById(R.id.btn_login);
        register = findViewById(R.id.btn_register);

         login.setOnClickListener(this);
        register.setOnClickListener(this);





    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_login:

                String  userName =  username.getText().toString().trim();
                if (TextUtils.isEmpty(userName)){
                    TipDialog.show("用户名不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }
                String password =  pwd.getText().toString().trim();
                if (TextUtils.isEmpty(password)){
                    TipDialog.show("密码不能为空", WaitDialog.TYPE.ERROR,2000);
                    return;
                }
                WaitDialog.show("请等待...",0.1f);
                //开始登录
                UserInfo userInfo = new UserInfo();
                userInfo.setUsername(userName);
                userInfo.setPwd(password);

               UserInfo newUserinfo = mUserDao.login(userInfo);

                if (newUserinfo!=null) {
                    WaitDialog.dismiss();
                    MyApplication.sUserInfo = newUserinfo;
                    Log.d("获取的数据",newUserinfo.toString());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }else{
                    WaitDialog.dismiss();
                    TipDialog.show("用户名或者密码错误", WaitDialog.TYPE.ERROR,2000);
                }


                break;
            case R.id.btn_register:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);

                break;
        }

    }

    //申请读写权限
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            final Context context = getApplicationContext();
            int readPermissionCheck = ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            int writePermissionCheck = ContextCompat.checkSelfPermission(context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (readPermissionCheck == PackageManager.PERMISSION_GRANTED
                    && writePermissionCheck == PackageManager.PERMISSION_GRANTED) {
                Log.v("juno", "Permission is granted");
                return true;
            } else {
                Log.v("juno", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("juno", "Permission is granted");
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.v("juno","onRequestPermissionsResult requestCode ： " + requestCode
                + " Permission: " + permissions[0] + " was " + grantResults[0]
                + " Permission: " + permissions[1] + " was " + grantResults[1]
        );
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                init();
        }
        SharedPreferencesHelper.init(getApplicationContext());

        int version = (int) SharedPreferencesHelper.getInstance().getData("version",MyApplication.dataBaseVersion);

        MyApplication.dataBaseVersion = version;
        DictionSQLiteOpenHelper dictionSQLiteOpenHelper = new DictionSQLiteOpenHelper(this,"ordermeal.db",null, MyApplication.dataBaseVersion);
        dictionSQLiteOpenHelper.getWritableDatabase().close();

        mUserDao = new UserDao(dictionSQLiteOpenHelper);
    }
}
